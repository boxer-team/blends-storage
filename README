# Storage Blend install routines

This document documents routines
to build and install the [Storage Blend][] of Debian using Boxer.

The routines themselves are avaiable in a [git repository][].

[Storage Blend]: <https://wiki.debian.org/DebianStorage>
  "Storage Blend wiki page"

[git repository]: <https://anonscm.debian.org/cgit/blend-storage/blend.git>
  "Storage Blend git repository"


## Preparations

### Environment

Build environment should be Debian Sid or Stretch,
with these packages installed:

    apt install curl make boxer

Other environments may work too,
just please mention loudly any deviation when reporting bugs.

Root access is *not* required to build the blend,
only for above package installation
and for executing the built blending recipe.


### This project

If you have not already done so,
fetch this project and move into it:

    git clone https://anonscm.debian.org/git/blend-storage/blend
    cd blend


## Install

Target system can be Debian Jessie, Stretch or Sid.

Documentation describes routines Debian Jessie
- adjust as needed for other suite.


### Manual

Debian installation is by default interactive.

  * Install Debian stable - see e.g. <https://www.debian.org/distrib/>.

  * Create install script:

        make clean
        make content/single-jessie/script.sh

  * Transfer content/single-jessie/script.sh to box,
    and execute it as root.


## Source

When this README file is accompagnied by binary installer images, the
following applies to satisfy license requirements:

I hereby offer to provide the source code for the relevant Debian binary
packages, included in the installer, on request.  However, you will
probably find it easier to acquire these packages from the official
Debian resources, ftp.debian.org and/or snapshot.debian.org.

Jonas Smedegaard <dr@jones.dk>  Fri, 01 Jan 2016 12:50:16 +0530
